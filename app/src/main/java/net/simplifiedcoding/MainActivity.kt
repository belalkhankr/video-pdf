package net.simplifiedcoding

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val PERMISSION_READ_STORAGE_CODE = 101
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_video.setOnClickListener {
            startActivity(Intent(applicationContext, VideoActivity::class.java))
        }

        button_pdf.setOnClickListener {

            if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@MainActivity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    Toast.makeText(this@MainActivity, "Give storage permission first...", Toast.LENGTH_LONG).show()
                    val myAppSettings =
                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:$packageName"))
                    myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                    myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
                    startActivity(myAppSettings)
                } else {
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        PERMISSION_READ_STORAGE_CODE
                    )
                }
            } else {
                startActivity(Intent(applicationContext, PDFActivity::class.java))
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_READ_STORAGE_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startActivity(Intent(applicationContext, PDFActivity::class.java))
                } else {
                    Toast.makeText(this@MainActivity, "Give storage permission first...", Toast.LENGTH_LONG).show()
                }
                return
            }
            else -> {
            }
        }
    }
}
