package net.simplifiedcoding

import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PixelFormat
import android.os.Bundle
import android.view.SurfaceHolder
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_pdf.*

class PDFActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf)

        val myIntent = Intent(Intent.ACTION_GET_CONTENT).apply{
            type = "*/*"
        }
        startActivityForResult(myIntent, 100)

        surfaceview_pdf.setSecure(true)
        surfaceview_pdf.setZOrderOnTop(true)
        surfaceview_pdf.holder.setFormat(PixelFormat.TRANSLUCENT)
        surfaceview_pdf.holder.addCallback(object: SurfaceHolder.Callback{
            override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

            }

            override fun surfaceDestroyed(holder: SurfaceHolder?) {

            }

            override fun surfaceCreated(holder: SurfaceHolder?) {
                var canvas: Canvas? = null
                try {
                    canvas = holder?.lockCanvas(null)
                    synchronized(holder!!) {
                        val paint = Paint()
                        paint.color = Color.RED
                        paint.textSize = 24F
                        canvas?.drawText("You Cannot Capture this Screen", 10F, 30F, paint)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    if (canvas != null) {
                        holder?.unlockCanvasAndPost(canvas)
                    }
                }
            }

        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 100){
            data?.let{
                pdfView.fromUri(it.data).load()
            }
        }
    }



}