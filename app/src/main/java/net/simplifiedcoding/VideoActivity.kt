package net.simplifiedcoding

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.SurfaceHolder
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_video.*


class VideoActivity : AppCompatActivity(), SurfaceHolder.Callback {

    private var mp: MediaPlayer? = null
    private var holder: SurfaceHolder? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        surfaceview_video.setSecure(true)

        holder = surfaceview_video.holder
        holder?.addCallback(this)

    }

    override fun onPause() {
        super.onPause()
        mp?.release()
    }


    override fun surfaceCreated(holder: SurfaceHolder) {
        mp?.setDisplay(holder)
        play()
    }


    override fun surfaceChanged(
        holder: SurfaceHolder, format: Int, width: Int,
        height: Int
    ) {
    }

    override fun onStart() {
        super.onStart()
        mp = MediaPlayer().apply {
            setOnCompletionListener {
                start()
            }
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        mp?.release()
        mp = null
    }

    private fun play() {
        val path = "android.resource://" + packageName + "/" + R.raw.sample
        try {
            mp?.setDataSource(this, Uri.parse(path))
            mp?.prepare()
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
        mp?.start()
    }
}
